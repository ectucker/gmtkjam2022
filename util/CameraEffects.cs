﻿using Godot;

/// <summary>
/// Provides common camera effects, such as screenshake and hitstop.
/// </summary>
public class CameraEffects : Node
{
    /// <summary>
    /// The singleton instance of these camera effects.
    /// </summary>
    public static CameraEffects Instance { get; private set; }

    private Vector3 _screenshakeRotation = Vector3.Zero;
    private float _kickbackLength;

    /// <summary>
    /// The current offset calculated by these effects. Should be applied to any camera using them.
    /// </summary>
    public Vector3 Rotation { get; private set; } = Vector3.Zero;

    /// <summary>
    /// The trauma value used for screenshake. Add on for an impact effect.
    /// </summary>
    public float Trauma { get; set; }

    private float _time = 0;

    private OpenSimplexNoise _noise;

    private float _hitstopEndTime = 0;

    public override void _Ready()
    {
        base._Ready();

        Instance = this;

        _noise = new OpenSimplexNoise();
        _noise.Octaves = 4;
        _noise.Period = 1.0f;
        _noise.Persistence = 0.8f;

        PauseMode = PauseModeEnum.Process;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _time += delta;
        Trauma = Mathf.Clamp(Trauma - delta, 0, 1);
        _kickbackLength = Mathf.Clamp(_kickbackLength - delta * 64.0f, 0, Mathf.Inf);

        _screenshakeRotation.x = Trauma * Trauma * _noise.GetNoise1d(_time);
        _screenshakeRotation.y = Trauma * Trauma * _noise.GetNoise1d(_time);
        _screenshakeRotation.z = Trauma * Trauma * _noise.GetNoise1d(_time);

        Rotation = _screenshakeRotation;

        if (OS.GetTicksMsec() > _hitstopEndTime)
            Engine.TimeScale = 1.0f;
    }

    /// <summary>
    /// Freeze the game for a duration in seconds.
    /// This works by changing Engine.TimeScale.
    /// </summary>
    /// <param name="duration">The duration to freeze for</param>
    public void Hitstop(float duration)
    {
        Engine.TimeScale = 0.0f;
        _hitstopEndTime = OS.GetTicksMsec() + duration * 1000;
    }
}
