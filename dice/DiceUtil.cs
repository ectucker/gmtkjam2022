﻿public static class DiceUtil
{
    public static DiceSize FromGrowth(float growth)
    {
        if (growth < 2)
            return DiceSize.D4;
        else if (growth < 3)
            return DiceSize.D6;
        else if (growth < 4)
            return DiceSize.D8;
        else if (growth < 5)
            return DiceSize.D10;
        else if (growth < 6)
            return DiceSize.D12;
        else
            return DiceSize.D20;
    }

    public static DiceSize FromNum(int num)
    {
        if (num <= 4)
            return DiceSize.D4;
        else if (num <= 6)
            return DiceSize.D6;
        else if (num <= 8)
            return DiceSize.D8;
        else if (num <= 10)
            return DiceSize.D10;
        else if (num <= 12)
            return DiceSize.D12;
        else
            return DiceSize.D20;
    }

    public static int ToNum(DiceSize size)
    {
        switch (size)
        {
            case DiceSize.D4:
                return 4;
            case DiceSize.D6:
                return 6;
            case DiceSize.D8:
                return 8;
            case DiceSize.D10:
                return 10;
            case DiceSize.D12:
                return 12;
            default:
                return 20;
        }
    }
}
