﻿using Godot;

public class Dice
{
    public DiceSize Size { get; private set; }

    public Dice(DiceSize size)
    {
        Size = size;
    }

    public DiceRoll Roll()
    {
        int val = (int) (GD.Randi() % DiceUtil.ToNum(Size)) + 1;
        var roll = new DiceRoll {Amount = val, Min = val == 1, Max = val == DiceUtil.ToNum(Size)};
        return roll;
    }
}

public class DiceRoll
{
    public int Amount;
    public bool Max;
    public bool Min;
}
