﻿using System.Threading.Tasks;
using Godot;

public class Projectile : Spatial, IResetable
{
    public static float INITIAL_SPEED = 9.0f;
    public static float HOMING_SPEED = 6.0f;
    
    private Dice _dice;
    public Vector3 InitialDir;
    public BasicEnemy Target;
    private AudioStreamPlayer3D _hit;

    public Dice Dice
    {
        get => _dice;
        set
        {
            _dice = value;
            (FindNode(DiceUtil.ToNum(_dice.Size).ToString()) as Spatial).Visible = true;
        }
    }

    public override void _Ready()
    {
        base._Ready();

        AsyncRoutine.Start(this, ProjectileAnimation);
        GetNode<Area>("HitArea").Connect("body_entered", this, nameof(_BodyEntered));
        _hit = GetNode<AudioStreamPlayer3D>("Hit");
        AddToGroup(Groups.RESET);
    }

    private async Task ProjectileAnimation(AsyncRoutine routine)
    {
        float time = 0;
        while (time < 0.7f)
        {
            float delta = await routine.IdleFrame();
            time += delta;
            Translation += InitialDir * delta * INITIAL_SPEED;
        }

        while (TargetValid())
        {
            float delta = await routine.IdleFrame();
            Translation += (Target.GlobalTransform.origin - Vector3.Down * 0.5f - Translation).Normalized() * HOMING_SPEED * delta;
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        
        Rotate(new Vector3(1, 1, 0).Normalized(), delta * 5.0f);
        
        if (!TargetValid())
            AsyncRoutine.Start(this, Ragdoll);
    }

    private bool TargetValid()
    {
        return Target != null && IsInstanceValid(Target) && Target.IsInsideTree();
    }

    private void _BodyEntered(PhysicsBody body)
    {
        if (body is BasicEnemy enemy)
        {
            _hit.Play();
            enemy.Damage(Dice.Roll());
            AsyncRoutine.Start(this, Ragdoll);
            CameraEffects.Instance.Hitstop(0.1f);
        }
    }

    private async Task Ragdoll(AsyncRoutine routine)
    {
        PackedScene ragdollScene = GD.Load<PackedScene>($"res://dice/ragdolls/{DiceUtil.ToNum(Dice.Size)}.tscn");
        Spatial ragdoll = ragdollScene.Instance<Spatial>();
        GetParent().AddChild(ragdoll);
        ragdoll.GlobalTransform = GlobalTransform;
        Visible = false;
        SetProcess(false);

        await routine.Delay(0.3f);
        
        QueueFree();
    }

    public void Reset()
    {
        QueueFree();
    }
}
