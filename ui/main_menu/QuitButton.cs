using Godot;
using System;

public class QuitButton : Button
{
    public override void _Ready()
    {
        base._Ready();

        Connect("pressed", this, nameof(_Pressed));
        Connect("mouse_entered", this, nameof(_Hovered));
    }

    private void _Pressed()
    {
        GlobalSounds.UIClick.Play();
        GetTree().Quit();
    }

    private void _Hovered()
    {
        GlobalSounds.UIHover.Play();
    }
}
