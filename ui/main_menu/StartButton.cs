using Godot;
using System;

public class StartButton : Button
{
    public override void _Ready()
    {
        base._Ready();

        Connect("pressed", this, nameof(_Pressed));
        Connect("mouse_entered", this, nameof(_Hovered));
    }

    private void _Pressed()
    {
        GlobalSounds.UIClick.Play();
        GameStateManager.Instance.GotoState(GameState.GAMEPLAY);
    }

    private void _Hovered()
    {
        GlobalSounds.UIHover.Play();
    }
}