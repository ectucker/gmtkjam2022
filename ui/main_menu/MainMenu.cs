using Godot;
using System;

public class MainMenu : Control
{
	public override void _Ready()
	{
		GameStateManager.Instance.Connect(nameof(GameStateManager.StateChanged), this, nameof(_StateChanged));
	}

	private void _StateChanged()
	{
		if (GameStateManager.Instance.State == GameState.MAIN_MENU)
			Visible = true;
		else
			Visible = false;
	}
}
