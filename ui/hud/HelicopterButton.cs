using Godot;
using System;

public class HelicopterButton : Button
{
    private AudioStreamPlayer _enabled;
    private AudioStreamPlayer _disabled;

    public override void _Ready()
    {
        base._Ready();
        
        Connect("toggled", this, nameof(_Toggled));
        _disabled = GetNode<AudioStreamPlayer>("Disabled");
        _enabled = GetNode<AudioStreamPlayer>("Enabled");
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        //Player.Instance.HelicopterOn = Pressed;
    }

    private void _Toggled(bool down)
    {
        if (down)
        {
            _enabled.Play();
        }
        else
        {
            _disabled.Play();
        }
    }
}
