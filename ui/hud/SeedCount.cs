using Godot;
using System;

public class SeedCount : Label
{
    public override void _Process(float delta)
    {
        base._Process(delta);

        Text = Player.Instance.Seeds.ToString("00");
    }
}
