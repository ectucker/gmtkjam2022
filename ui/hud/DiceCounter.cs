using Godot;
using System;

public class DiceCounter : Control
{
    [Export]
    public int Dice = 4;

    private DiceSize _size;

    private Label _count;

    public override void _Ready()
    {
        base._Ready();
        
        _size = DiceUtil.FromNum(Dice);
        GetNode<Label>("TempLabel").Text = DiceUtil.ToNum(_size).ToString();
        _count = GetNode<Label>("Count");
        Inventory.Instance.Connect(nameof(Inventory.Changed), this, nameof(UpdateDisplay));

        TextureRect icon = this.FindChild<TextureRect>();
        icon.Texture = GD.Load<Texture>($"res://ui/hud/d{DiceUtil.ToNum(_size)}.png");
    }

    private void UpdateDisplay()
    {
        _count.Text = Inventory.Instance.Count(_size).ToString("00");
    }
}
