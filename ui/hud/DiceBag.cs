using Godot;
using System;

public class DiceBag : TextureButton
{
    private AudioStreamPlayer _pickupDice;
    
    public override void _Ready()
    {
        Connect("pressed", this, nameof(Pressed));
        _pickupDice = GetNode<AudioStreamPlayer>("PickupDice");
    }

    private void Pressed()
    {
        var dice = Inventory.Instance.PickDice();
        if (dice.Count > 0)
        {
            Player.Instance.HeldDice = dice;
            _pickupDice.Play();
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        Disabled = Inventory.Instance.CountAll() == 0;
    }
}
