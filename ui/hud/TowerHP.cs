using Godot;
using System;

public class TowerHP : Control
{
    private const string HEART_PATH = "res://ui/hud/tower_heart.tscn";
    
    public override void _Process(float delta)
    {
        base._Process(delta);

        if (GetChildCount() != DiceTower.Instance.Health)
        {
            foreach (Node child in GetChildren())
            {
                RemoveChild(child);
            }

            var scene = GD.Load<PackedScene>(HEART_PATH);
            for (int i = 0; i < DiceTower.Instance.Health; i++)
            {
                AddChild(scene.Instance<Control>());
            }
        }
    }
}
