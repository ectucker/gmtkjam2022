using Godot;
using System;

public class DefeatScreen : Control
{
    public override void _Ready()
    {
        GameStateManager.Instance.Connect(nameof(GameStateManager.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        if (GameStateManager.Instance.State == GameState.GAME_OVER)
            Visible = true;
        else
            Visible = false;
    }
}