using Godot;
using System;

public abstract class Crop : Spatial, IResetable
{
    public virtual float MaxGrowth => 7.0f;
    public virtual float GrowthRate => 1.0f;
    
    public float Growth { get; private set; }

    protected PlantingTile _tile;

    public override void _Ready()
    {
        base._Ready();

        _tile = this.FindParent<PlantingTile>();
        AddToGroup(Groups.RESET);
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        Growth = Mathf.Clamp(Growth + delta * GrowthRate * _tile.GrowthModifier, 0, MaxGrowth);
        Scale = new Vector3(Growth / MaxGrowth, Growth / MaxGrowth, Growth / MaxGrowth);
    }

    public void Harvest()
    {
        HarvestData();
        QueueFree();
    }
    
    /// <summary>
    /// Called when harvesting the crop. Should only deal with putting the harvest in the inventory.
    /// </summary>
    public abstract void HarvestData();

    public abstract bool CanHarvest();
    public void Reset()
    {
        QueueFree();
    }
}
