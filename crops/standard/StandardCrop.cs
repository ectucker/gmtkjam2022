using Godot;
using System;

public class StandardCrop : Crop
{
    private Spatial _dice;

    public override void _Ready()
    {
        base._Ready();

        _dice = GetNode<Spatial>("Dice");
    }

    public override void HarvestData()
    {
        int count = Mathf.FloorToInt(1 * _tile.YieldModifier);
        for (int i = 0; i < count; i++)
        {
            Dice dice = new Dice(DiceUtil.FromGrowth(Growth));
            Inventory.Instance.Add(dice);
        }
    }

    public override bool CanHarvest()
    {
        return Growth >= 1.0f;
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        
        _dice.Rotate(new Vector3(1, 1, 0).Normalized(), delta * 0.5f);
        if (Growth >= 1.0f)
        {
            DiceSize size = DiceUtil.FromGrowth(Growth);
            {
                foreach (Spatial child in _dice.GetChildren())
                    child.Visible = false;
                _dice.GetNode<Spatial>(DiceUtil.ToNum(size).ToString()).Visible = true;
            }
        }
    }
}
