using Godot;
using System;

public class BasicEnemy : RigidBody, IResetable
{
    private PackedScene _hit;
    
    public static float JUMP_IMPULSE = 4.0f;
    public static float MAX_SPEED = 0.05f;
    public static float ACCEL = 1.0f;
    
    public static float TURN_STOP_THRESHOLD = 0.2f;
    public static float TURN_SPEED = 5.0f;
    
    private RayCast _jumpCast;
    private MeshInstance _mesh;

    private float _lastJumped = Mathf.Inf;

    private int _health = 10;

    public int Health
    {
        get => _health;
        set
        {
            _health = value;
            UpdateMaterialTexture();
        }
    }

    public override void _Ready()
    {
        base._Ready();

        _jumpCast = GetNode<RayCast>("JumpCast");
        AddToGroup(Groups.ENEMY);
        _mesh = this.FindChild<MeshInstance>();
        _mesh.MaterialOverride = _mesh.GetActiveMaterial(0).Duplicate() as Material;
        UpdateMaterialTexture();
        AddToGroup(Groups.RESET);
        _hit = GD.Load<PackedScene>("res://enemy/hit.tscn");
    }

    private void UpdateMaterialTexture()
    {
        if (_mesh.MaterialOverride is SpatialMaterial mat)
        {
            if (_health >= 1 && _health <= 13)
            {
                mat.AlbedoTexture = GD.Load<Texture>($"res://enemy/basic/CardTextures/{_health}CardTexture.png");
            }
            else if (_health == 0)
            {
                mat.AlbedoTexture = GD.Load<Texture>($"res://enemy/basic/CardTextures/1CardTexture.png");
            }
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);

        Vector2 targetDiff = new Vector2(-GlobalTransform.origin.x, -GlobalTransform.origin.z);
        float angleDiff = Mathf.Wrap(Mathf.Atan2(targetDiff.x, targetDiff.y) - GlobalTransform.basis.GetEuler().y,
            -Mathf.Pi, Mathf.Pi);
        if (Mathf.Abs(angleDiff) > TURN_STOP_THRESHOLD)
            GlobalRotate(Vector3.Up, TURN_SPEED * delta * Mathf.Sign(angleDiff));

        if (new Vector2(LinearVelocity.x, LinearVelocity.z).LengthSquared() < MAX_SPEED * MAX_SPEED)
        {
            Vector2 impulse = ACCEL * targetDiff.Normalized();
            ApplyCentralImpulse(new Vector3(impulse.x, 0, impulse.y));
        }

        _lastJumped += delta;
        if (_jumpCast.IsColliding())
        {
            if (_jumpCast.GetCollisionPoint().y > GlobalTransform.origin.y && _lastJumped > 0.2f)
            {
                Jump();
            }

            if (_jumpCast.GetCollider() is DiceTower tower)
            {
                CameraEffects.Instance.Hitstop(0.1f);
                CameraEffects.Instance.Trauma += 0.8f;
                Spatial hit = _hit.Instance<Spatial>();
                GetParent().AddChild(hit);
                hit.GlobalTransform = GlobalTransform;
                tower.Damage();
                Kill();
            }
        }

        if (_lastJumped > 0.2f && GD.Randf() < 0.01)
        {
            Jump();
        }
    }

    private void Jump()
    {
        ApplyCentralImpulse(new Vector3(0, JUMP_IMPULSE, 0));
        _lastJumped = 0;
    }

    public void Damage(DiceRoll amount)
    {
        Health = Mathf.Clamp(Health - amount.Amount, 0, 2000);
        PackedScene markerScene = GD.Load<PackedScene>("res://base/hitmarker.tscn");
        Hitmarker marker = markerScene.Instance<Hitmarker>();
        GetParent().AddChild(marker);
        marker.Translation = GlobalTransform.origin + Vector3.Up * .2f;
        marker.Text = amount.Amount.ToString();
        if (Health <= 0)
            Kill();
    }

    public void Kill()
    {
        PackedScene ragdollScene = GD.Load<PackedScene>($"res://enemy/basic/enemy_ragdoll.tscn");
        Spatial ragdoll = ragdollScene.Instance<Spatial>();
        GetParent().AddChild(ragdoll);
        ragdoll.GlobalTransform = GlobalTransform;
        Player.Instance.Seeds += 2;
        QueueFree();
    }

    public void Reset()
    {
        QueueFree();
    }
}
