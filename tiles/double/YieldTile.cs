using Godot;
using System;

public class YieldTile : PlantingTile
{
    public override float YieldModifier => 2.0f;
    public override float GrowthModifier => 0.75f;
}
