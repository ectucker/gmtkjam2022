using Godot;
using System;

public abstract class PlantingTile : StaticBody, IResetable
{
    public virtual bool Plantable => true;
    public virtual float GrowthModifier => 1.0f;
    public virtual float YieldModifier => 1.0f;

    public bool Planted = false;

    private Spatial _cropRoot;

    public Optional<Crop> Crop
    {
        get
        {
            var crop = this.FindChild<Crop>();
            if (crop != null)
                return Optional<Crop>.Some(crop);
            else
                return Optional<Crop>.None;
        }
    }

    public override void _Ready()
    {
        base._Ready();

        _cropRoot = GetNode<Spatial>("CropRoot");
        if (_cropRoot == null)
            _cropRoot = this;
        AddToGroup(Groups.RESET);
    }

    public void Plant(string path)
    {
        var cropScene = GD.Load<PackedScene>(path);
        var crop = cropScene.Instance<Crop>();
        AddChild(crop);
    }

    public void Reset()
    {
        Planted = false;
    }
}
