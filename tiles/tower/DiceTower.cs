using Godot;
using System;
using System.Collections.Generic;

public class DiceTower : StaticBody, IResetable
{
    private const string PROJECTILE_PATH = "res://dice/dice_projectile.tscn";
    
    public static float FIRE_RATE = 0.2f;
    
    public static DiceTower Instance;

    private List<Dice> _queudDice = new List<Dice>();

    private float _lastFired = Mathf.Inf;

    private Spatial _spawn;
    private Spatial _dir;

    private List<BasicEnemy> _targetQueue = new List<BasicEnemy>();

    private AudioStreamPlayer3D _diceTower;
    private AudioStreamPlayer3D _shoot;

    private const int StartingHealth = 5;
    private int _health = StartingHealth;

    public int Health
    {
        get => _health;
        set
        {
            _health = value;
            if (value > 0)
            {
                GetParent<Spatial>().Visible = true;
                _brokenVersion.Visible = false;
            }
            else
            {
                GetParent<Spatial>().Visible = false;
                _brokenVersion.Visible = true;
            }
        }
    }

    private Spatial _brokenVersion;
    
    public override void _Ready()
    {
        base._Ready();

        Instance = this;
        _spawn = GetNode<Spatial>("ProjectileSpawn");
        _dir = GetNode<Spatial>("ProjectileDir");
        _diceTower = FindNode("DiceTower") as AudioStreamPlayer3D;
        _shoot = FindNode("Shoot") as AudioStreamPlayer3D;
        AddToGroup(Groups.RESET);
        _brokenVersion = GetParent().GetParent().GetNode<Spatial>("brokenTower");
    }

    public void QueueDice(List<Dice> _dice)
    {
        _queudDice.AddRange(_dice);
        _diceTower.Play();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _lastFired += delta;
        if (_queudDice.Count > 0 && _lastFired > FIRE_RATE)
        {
            Dice next = _queudDice[0];
            _queudDice.RemoveAt(0);
            FireDice(next);
        }
    }

    private BasicEnemy GetTarget()
    {
        while (_targetQueue.Count > 0)
        {
            var target = _targetQueue[0];
            _targetQueue.RemoveAt(0);
            if (IsInstanceValid(target) && target.IsInGroup("Enemies"))
                return target;
        }
        
        foreach (var enemy in GetTree().GetNodesInGroup("Enemies"))
        {
            _targetQueue.Add(enemy as BasicEnemy);
        }

        if (_targetQueue.Count == 0)
            return null;
        var target2 = _targetQueue[0];
        _targetQueue.RemoveAt(0);
        return target2;
    }
    
    private void FireDice(Dice dice)
    {
        BasicEnemy _target = GetTarget();
        
        PackedScene scene = GD.Load<PackedScene>(PROJECTILE_PATH);
        var proj = scene.Instance<Projectile>();
        GetParent().GetParent().GetParent().AddChild(proj);
        proj.Translation = _spawn.Translation;
        proj.InitialDir = (_dir.GlobalTransform.origin - _spawn.GlobalTransform.origin).Normalized();
        proj.Dice = dice;
        proj.Target = _target;
        
        _shoot.Play();
        CameraEffects.Instance.Trauma += 0.2f;
    }

    public void Damage()
    {
        Health -= 1;
        if (Health <= 0)
            GameStateManager.Instance.GotoState(GameState.GAME_OVER);
    }

    public void Reset()
    {
        _queudDice.Clear();
        Health = StartingHealth;
    }
}
