using Godot;
using System;

public class TowerSpin : Spatial
{
    public override void _Process(float delta)
    {
        base._Process(delta);
        
        RotateY(delta * 1.0f);
    }
}
