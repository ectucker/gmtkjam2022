using Godot;
using System;

public class TileHighlight : Spatial
{
    private MeshInstance _mesh;

    public Color Color
    {
        set
        {
            if (_mesh.GetActiveMaterial(0) is SpatialMaterial spatialMaterial)
            {
                spatialMaterial.AlbedoColor = value;
            }
        }
    }
    
    public override void _Ready()
    {
        _mesh = this.FindChild<MeshInstance>();
    }
}
