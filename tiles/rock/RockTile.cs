using Godot;
using System;

public class RockTile : PlantingTile
{
    public override bool Plantable => false;
}
