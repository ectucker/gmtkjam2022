using Godot;
using System;

public class GrowthTile : PlantingTile
{
    public override float GrowthModifier => 1.3f;
}
