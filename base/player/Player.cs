using Godot;
using System;
using System.Collections.Generic;

public class Player : Spatial, IResetable
{
    public static Player Instance;
    private const float RAY_LENGTH = 1000.0f;

    private const string STANDARD_CROP_PATH = "res://crops/standard/standard_crop.tscn";
    private const string DRAG_PATH = "res://base/player/dice_drag.tscn";
    
    private Camera _camera;

    private DiceDrag _drag;

    private List<Dice> _heldDice = null;
    public List<Dice> HeldDice
    {
        get => _heldDice;
        set
        {
            if (value != null && value.Count > 0)
            {
                _heldDice = value;
                var dragScene = GD.Load<PackedScene>(DRAG_PATH);
                _drag = dragScene.Instance<DiceDrag>();
                _drag.HeldDice = _heldDice;
                AddChild(_drag);
            }
            else
            {
                if (_drag != null)
                {
                    _drag.QueueFree();
                    _drag = null;
                }

                _heldDice = null;
            }
        }
    }

    private TileHighlight _tileHighlight;

    private const uint startingSeeds = 5;
    public uint Seeds = startingSeeds;

    public override void _Ready()
    {
        base._Ready();

        _camera = this.FindChild<Camera>();
        Instance = this;
        _tileHighlight = GetNode<TileHighlight>("Highlight");
        AddToGroup(Groups.RESET);
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);
        
        HoverInfo info = GetTarget();
        if (info.HasTarget && info.Target is PlantingTile tile && tile.Plantable)
        {
            if (tile.Crop.IsNone)
            {
                _tileHighlight.Translation = tile.GlobalTransform.origin;
                _tileHighlight.Visible = true;
                if (Seeds > 0)
                    _tileHighlight.Color = Colors.Yellow;
                else
                    _tileHighlight.Color = Colors.Gray;
            }
            else if (tile.Crop.IsSome && tile.Crop.Value.CanHarvest())
            {
                _tileHighlight.Translation = tile.GlobalTransform.origin;
                _tileHighlight.Visible = true;
                _tileHighlight.Color = Colors.Red;
            }
            else
            {
                _tileHighlight.Visible = false;
            }
        }
        else
        {
            _tileHighlight.Visible = false;
        }
    }

    public override void _Input(InputEvent input)
    {
        base._Input(input);

        if (input.IsActionPressed("action_interact"))
        {
            HoverInfo info = GetTarget();
            if (info.HasTarget)
            {
                if (info.Target is PlantingTile tile)
                {
                    if (tile.Plantable)
                    {
                        var crop = tile.Crop;
                        if (crop.IsNone)
                        {
                            if (Seeds > 0)
                            {
                                tile.Plant(STANDARD_CROP_PATH);
                                Seeds--;
                            }
                        }
                    }
                }
            }
        }

        if (input.IsActionReleased("action_interact"))
        {
            if (HeldDice != null)
            {
                HoverInfo info = GetTarget();
                if (info.HasTarget && info.Target is DiceTower tower)
                {
                    if (_drag != null)
                    {
                        _drag.GoToTower();
                        _drag = null;
                    }

                    //tower.QueueDice(HeldDice);
                    HeldDice = null;
                }
                else
                {
                    foreach (var dice in HeldDice)
                    {
                        Inventory.Instance.Add(dice);
                    }

                    HeldDice = null;
                }
            }
        }
    }

    public HoverInfo GetTarget()
    {
        HoverInfo info = new HoverInfo();
        var mousePos = GetViewport().GetMousePosition();
        var rayFrom = _camera.ProjectRayOrigin(mousePos);
        var rayTo = rayFrom + _camera.ProjectRayNormal(mousePos) * RAY_LENGTH;
        var spaceState = GetWorld().DirectSpaceState;
        var selection = spaceState.IntersectRay(rayFrom, rayTo, collisionMask: 2);
        if (selection.Contains("collider"))
        {
            info.HasTarget = true;
            info.Target = selection["collider"] as PhysicsBody;
            info.Position = (Vector3)selection["position"];
        }
        else
        {
            info.HasTarget = false;
        }

        return info;
    }

    public void Reset()
    {
        Seeds = startingSeeds;
        _heldDice = null;
    }
}
