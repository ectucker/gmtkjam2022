using Godot;
using System;

public class AutoplayChildAnim : Spatial
{
    private AnimationPlayer _player;
    private string _anim;
    
    public override void _Ready()
    {
        _player = this.FindChild<AnimationPlayer>();
        _anim = _player.GetAnimationList()[0];
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        
        _player.PlayIfNot(_anim);
    }
}
