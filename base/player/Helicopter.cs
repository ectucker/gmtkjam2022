using Godot;
using System;
using System.Collections.Generic;

public class Helicopter : Spatial
{
    public static float MOVE_STOP_THRESHOLD = 0.1f;
    public static float TURN_STOP_THRESHOLD = 0.1f;
    public static float TURN_SPEED = 1.5f;
    public static float MOVE_SPEED = 1.5f;
    
    private Player _player;

    private List<PlantingTile> _harvestQueue = new List<PlantingTile>();

    private AudioStreamPlayer3D _harvestSound;
    
    public override void _Ready()
    {
        _player = this.FindParent<Player>();
        _harvestSound = GetNode<AudioStreamPlayer3D>("HarvestSound");
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);


        Vector2 diff = GetTargetPos() - new Vector2(GlobalTransform.origin.x, GlobalTransform.origin.z);
        if (diff.Length() > MOVE_STOP_THRESHOLD)
        {
            float angleDiff = Mathf.Wrap(Mathf.Atan2(diff.x, diff.y) - GlobalTransform.basis.GetEuler().y,
                -Mathf.Pi, Mathf.Pi);
            if (Mathf.Abs(angleDiff) > TURN_STOP_THRESHOLD)
                GlobalRotate(Vector3.Up, TURN_SPEED * delta * Mathf.Sign(angleDiff) * Math.Max(angleDiff,1));
            Vector2 headMove = diff.Normalized() * MOVE_SPEED * delta * Math.Min(diff.Length()+(1/2),4);
            DebugOverlay.AddMessage(this, "AngleDiff", $"{angleDiff}");
            GlobalTranslate(new Vector3(headMove.x, 0, headMove.y));
        }
        else
        {
            if (NextCropTarget() != null)
            {
                NextCropTarget().Crop.Value.Harvest();
                _harvestSound.Play();
            }
        }
    }

    PlantingTile NextCropTarget()
    {
        while (_harvestQueue.Count > 0)
        {
            if (_harvestQueue[0].Plantable)
            {
                var crop = _harvestQueue[0].Crop;
                if (crop.IsSome && crop.Value.CanHarvest())
                {
                    return _harvestQueue[0];
                }
                else
                {
                    _harvestQueue.RemoveAt(0);
                }
            }
            else
            {
                _harvestQueue.RemoveAt(0);
            }
        }

        return null;
    }
    
    Vector2 GetTargetPos()
    {
        var cropTarget = NextCropTarget();
        if (cropTarget != null)
        {
            return new Vector2(cropTarget.GlobalTransform.origin.x, cropTarget.GlobalTransform.origin.z);
        }
        
        /*var info = _player.GetTarget();
        if (info.HasTarget)
        {
            return new Vector2(info.Position.x, info.Position.z);
        }*/

        return new Vector2(GlobalTransform.origin.x, GlobalTransform.origin.z);
    }
    
    public override void _Input(InputEvent input)
    {
        base._Input(input);

        if (input.IsActionPressed("action_interact"))
        {
            HoverInfo info = _player.GetTarget();
            if (info.HasTarget)
            {
                if (info.Target is PlantingTile tile)
                {
                    if (tile.Plantable)
                    {
                        var crop = tile.Crop;
                        if (crop.IsSome && crop.Value.CanHarvest())
                        {
                            if (!_harvestQueue.Contains(tile))
                                _harvestQueue.Add(tile);
                        }
                    }
                }
            }
        }
    }
}
