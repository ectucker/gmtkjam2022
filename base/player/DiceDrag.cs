using Godot;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class DiceDrag : Spatial, IResetable
{
    private const float DIST = 2.0f;
    
    private Player _player;
    private Camera _camera;

    private bool _dragging = true;

    private List<Dice> _heldDice;
    public List<Dice> HeldDice
    {
        get => _heldDice;
        set
        {
            _heldDice = value;
            foreach (var dice in _heldDice)
            {
                int val = DiceUtil.ToNum(dice.Size);
                PackedScene diceScene = GD.Load<PackedScene>($"res://dice/D{val}/D{val}.glb");
                Spatial diceSpatial = diceScene.Instance<Spatial>();
                AddChild(diceSpatial);
                diceSpatial.Scale = new Vector3(0.1f, 0.1f, 0.1f);
                diceSpatial.Translation = new Vector3(GD.Randf() * 0.1f, GD.Randf() * 0.1f, GD.Randf() * 0.1f);
            }
        }
    }

    public override void _Ready()
    {
        base._Ready();

        _player = this.FindParent<Player>();
        _camera = _player.FindChild<Camera>();
        AddToGroup(Groups.RESET);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_dragging)
        {
            var mousePos = GetViewport().GetMousePosition();
            var rayFrom = _camera.ProjectRayOrigin(mousePos);
            var rayTo = rayFrom + _camera.ProjectRayNormal(mousePos) * DIST;

            Transform newxform = GlobalTransform;
            newxform.origin = rayTo;
            GlobalTransform = newxform;
        }
        
        Rotate(new Vector3(1, 1, 0).Normalized(), delta * 5.0f);
    }

    public void GoToTower()
    {
        _dragging = false;
        AsyncRoutine.Start(this, TowerAnim);
    }

    private async Task TowerAnim(AsyncRoutine routine)
    {
        Tween tween = new Tween();
        AddChild(tween);
        tween.InterpolateProperty(this, "translation", Transform.origin, new Vector3(0, 2.5f, 0),
            0.8f, Tween.TransitionType.Quart, Tween.EaseType.Out);
        tween.Start();

        await routine.Delay(0.8f);

        while (Translation.y > 1.0f)
        {
            float delta = await routine.IdleFrame();
            Translation += Vector3.Down * delta * 9.8f;
        }
        
        DiceTower.Instance.QueueDice(HeldDice);
        QueueFree();
    }

    public void Reset()
    {
        QueueFree();
    }
}
