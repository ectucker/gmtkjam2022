﻿using Godot;

public class HoverInfo
{
    public bool HasTarget;
    public PhysicsBody Target;
    public Vector3 Position;
}
