using Godot;
using System;

public class CameraController : Spatial
{
    public static float ROTATE_SPEED = 0.7f;
    public static float ZOOM_SPEED = 1.5f;

    public static float MIN_ZOOM = 1.5f;
    public static float MAX_ZOOM = 7.0f;

    private Spatial _holder;
    private Camera _camera;

    public override void _Ready()
    {
        base._Ready();

        _holder = GetParentSpatial();
        _camera = GetChild<Camera>(0);
        GameStateManager.Instance.Connect(nameof(GameStateManager.StateChanged), this, nameof(StateChanged));
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        float cameraX = Input.GetActionStrength("camera_right") - Input.GetActionStrength("camera_left");
        float cameraZ = Input.GetActionStrength("camera_back") - Input.GetActionStrength("camera_forward");
        
        _holder.RotateY(cameraX * delta * ROTATE_SPEED);
        float newZ = Mathf.Clamp(Transform.origin.z + cameraZ * ZOOM_SPEED * delta, MIN_ZOOM, MAX_ZOOM);
        var xform = Transform;
        xform.origin.z = newZ;
        Transform = xform;
        
        _camera.Transform = Transform.Identity;
        _camera.RotateX(CameraEffects.Instance.Rotation.x * 0.2f);
        _camera.RotateY(CameraEffects.Instance.Rotation.y * 0.3f);
        _camera.RotateZ(CameraEffects.Instance.Rotation.z * 0.2f);
    }

    private void StateChanged()
    {
        if (GameStateManager.Instance.State == GameState.GAMEPLAY)
            _camera.Current = true;
    }
}
