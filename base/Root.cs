using Godot;
using System;

public class Root : Node
{
    [LiveValue(LVType.RANGE, 0, 100, "Test")]
    public static float TEST_VALUE = 5;
    
    public override void _Ready()
    {
        
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        
        DebugOverlay.AddMessage(this, "Test", "Message", Colors.Aqua);
    }
}
