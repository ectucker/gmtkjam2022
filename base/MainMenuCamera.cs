using Godot;
using System;

public class MainMenuCamera : Camera
{
    public override void _Ready()
    {
        GameStateManager.Instance.Connect(nameof(GameStateManager.StateChanged), this, nameof(_StateChanged));
    }

    private void _StateChanged()
    {
        if (GameStateManager.Instance.State != GameState.GAMEPLAY)
            Current = true;
    }
}
