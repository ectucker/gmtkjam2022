﻿using Godot;

public class ResetDelete : Spatial, IResetable
{
    public override void _Ready()
    {
        base._Ready();
        
        AddToGroup(Groups.RESET);
    }

    public void Reset()
    {
        QueueFree();
    }
}
