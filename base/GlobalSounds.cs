using Godot;
using System;

public class GlobalSounds : Node
{
    public static AudioStreamPlayer UIHover;
    public static AudioStreamPlayer UIClick;

    public override void _Ready()
    {
        base._Ready();

        UIHover = GetNode<AudioStreamPlayer>("UIHover");
        UIClick = GetNode<AudioStreamPlayer>("UIClick");
    }
}
