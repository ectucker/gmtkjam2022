using Godot;
using System;

public class EnemySpawner : Spatial, IResetable
{
    public static EnemySpawner Instance;
    
    private const string ENEMY_PATH = "res://enemy/basic/basic_enemy.tscn";
    private PackedScene _enemyScene;

    private static float WAVE_TIME = 15.0f;

    private const int LAST_WAVE = 7;
    
    private int Wave = 0;
    private float _time;
    
    public override void _Ready()
    {
        _enemyScene = GD.Load<PackedScene>(ENEMY_PATH);
        AddToGroup(Groups.RESET);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _time += delta;
        if (_time > WAVE_TIME)
        {
            Wave++;
            _time = 0;
            SpawnWave(Wave);
        }

        if (Wave > LAST_WAVE && GetTree().GetNodesInGroup(Groups.ENEMY).Count == 0)
        {
            GameStateManager.Instance.GotoState(GameState.VICTORY);
        }
    }

    private void SpawnWave(int wave)
    {
        if (wave > LAST_WAVE)
        {
            
        }
        else
        {
            for (int i = 0; i < wave; i++)
            {
                BasicEnemy enemy = _enemyScene.Instance<BasicEnemy>();
                AddChild(enemy);
                enemy.Translation = Vector3.Right.Rotated(Vector3.Up, GD.Randf() * Mathf.Tau) * 6.0f;
                enemy.Health = (int) Math.Min(wave + 1 + GD.Randi() % (wave + 3),13);
            }
        }
    }

    public void Reset()
    {
        Wave = 0;
        _time = 0;
    }
}
