using Godot;
using System;
using System.Collections.Generic;
using ExtraMath;

[Tool]
public class MapGen : Spatial
{
    private const string STANDARD_TILE_PATH = "res://tiles/standard/standard_tile.tscn";
    private const string INCREASED_YIELD_PATH = "res://tiles/double/increased_yield.tscn";
    private const string FASTER_GROWTH_PATH = "res://tiles/speed/faster_growth.tscn";
    private const string ROCK_TILE_PATH = "res://tiles/rock/rock_tile.tscn";

    [Export]
    public bool Generate
    {
        get => false;
        set
        {
            if (value)
                GenerateMap();
        }
    }

    private void GenerateMap()
    {
        PackedScene standardTile = GD.Load<PackedScene>(STANDARD_TILE_PATH);
        PackedScene rockTile = GD.Load<PackedScene>(ROCK_TILE_PATH);
        PackedScene yieldTile = GD.Load<PackedScene>(INCREASED_YIELD_PATH);
        PackedScene speedTile = GD.Load<PackedScene>(FASTER_GROWTH_PATH);

        foreach (Vector2i hex in HexsWithin(5))
        {
            PackedScene toPlace = standardTile;
            float roll = GD.Randf();
            if (roll < 0.25f)
                toPlace = rockTile;
            else if (roll < 0.5f)
                toPlace = standardTile;
            else if (roll < 0.75f)
                toPlace = yieldTile;
            else
                toPlace = speedTile;
            
            Spatial tile = toPlace.Instance<Spatial>();
            AddChild(tile, true);
            tile.Owner = GetTree().EditedSceneRoot;
            Vector3 start = HexCoordToCenter(hex.x, 0, hex.y);
            start.y = 1 - (Mathf.Sqrt(start.x * start.x + start.y * start.y) / 10) + GD.Randf() / 5;
            tile.GlobalTranslate(start);
        }
    }

    private static Vector3 HexCoordToCenter(int x, float y, int z)
    {
        if (z % 2 == 0)
        {
            return new Vector3(x, y, z * (Mathf.Sqrt(3) / 2));
        }
        else
        {
            return new Vector3(x + 0.5f, y, z * (Mathf.Sqrt(3) / 2));
        }
    }

    private static IEnumerable<Vector2i> HexsWithin(float radius)
    {
        for (int i = -20; i < 20; i++)
        {
            for (int j = -20; j < 20; j++)
            {
                if (i == 0 && j == 0)
                    continue;
                if (HexCoordToCenter(i, 0, j).LengthSquared() < radius * radius)
                    yield return new Vector2i(i, j);
            }
        }
    }
}
