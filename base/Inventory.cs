﻿using System.Collections.Generic;
using Godot;

public class Inventory : Node, IResetable
{
    public static Inventory Instance { get; private set; }

    private List<Dice> _dice = new List<Dice>();

    [Signal]
    public delegate void Changed();
    
    public override void _Ready()
    {
        base._Ready();

        Instance = this;
        AddToGroup(Groups.RESET);
    }

    public void Add(Dice dice)
    {
        _dice.Add(dice);
        EmitSignal(nameof(Changed));
    }
    
    public int Count(DiceSize size)
    {
        int count = 0;
        foreach (var dice in _dice)
            if (dice.Size == size)
                count++;
        return count;
    }

    public int CountAll()
    {
        return _dice.Count;
    }

    public List<Dice> PickDice()
    {
        int toPick = _dice.Count / 2;
        List<Dice> picked = new List<Dice>();
        for (int i = 0; i < Mathf.Ceil(_dice.Count / 2.0f); i++)
        {
            picked.Add(_dice[0]);
            _dice.RemoveAt(0);
        }
        EmitSignal(nameof(Changed));

        return picked;
    }

    public void Reset()
    {
        _dice.Clear();
    }
}