using Godot;
using System;

public class GameStateManager : Node
{
    public static GameStateManager Instance;

    public GameState State = GameState.MAIN_MENU;

    [Signal]
    public delegate void StateChanged();
    
    public override void _Ready()
    {
        base._Ready();
        
        Instance = this;
        GotoState(GameState.MAIN_MENU);
    }

    public void GotoState(GameState state)
    {
        State = state;
        switch (state)
        {
            case GameState.GAMEPLAY:
                Reset();
                GetTree().Paused = false;
                break;
            default:
                GetTree().Paused = true;
                break;
        }
        EmitSignal(nameof(StateChanged));
    }

    private void Reset()
    {
        foreach (var node in GetTree().GetNodesInGroup(Groups.RESET))
        {
            if (node is IResetable resetable)
                resetable.Reset();
        }
    }

    public override void _Input(InputEvent @event)
    {
        base._Input(@event);

        if (@event.IsActionPressed("toggle_fullscreen"))
        {
            OS.WindowFullscreen = !OS.WindowFullscreen;
        }
    }
}

public enum GameState
{
    MAIN_MENU, GAME_OVER, VICTORY, GAMEPLAY
}
