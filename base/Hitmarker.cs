using Godot;
using System;

public class Hitmarker : Spatial
{
    private Sprite3D _sprite;
    private Viewport _viewport;
    
    public string Text
    {
        set
        {
            Label label = this.FindChild<Label>();
            label.Text = value;
            _viewport.RenderTargetUpdateMode = Viewport.UpdateMode.Always;
            _sprite.Texture = _viewport.GetTexture();
        }
    }

    private float _time = 0;
    
    public override void _Ready()
    {
        base._Ready();

        _sprite = this.FindChild<Sprite3D>();
        _viewport = this.FindChild<Viewport>();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        _time += delta;
        if (_time > 0.5f)
            QueueFree();
        GlobalTranslate(Vector3.Up * delta * 0.3f);
    }
}
