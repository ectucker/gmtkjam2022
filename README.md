﻿# gmtkjam2022

A game made for Game Maker's Toolkit Game Jam 2022.

## Initial repository content

Before the start of the jam, the following content is in this repo:

- Basic Godot project
- GitLab Continuous Integration setup
- [EctGodotUtils](https://github.com/ectucker1/EctGodotUtils)
- [GodotExtraMath](https://github.com/aaronfranke/GodotExtraMath)
- [godot-plugin-refresher](https://github.com/godot-extended-libraries/godot-plugin-refresher)
